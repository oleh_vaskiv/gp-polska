<?php

namespace Ls\MenuBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface {
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder() {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('ls_menu');

        $rootNode
            ->addDefaultsIfNotSet()
            ->children()
            ->arrayNode('locations')
            ->prototype('array')
            ->children()
            ->scalarNode('label')->end()
            ->scalarNode('name')->end()
            ->end()
            ->end()
            ->end()
            ->arrayNode('modules')
            ->prototype('array')
            ->children()
            ->scalarNode('label')->end()
            ->scalarNode('route')->end()
            ->variableNode('route_parameters')->defaultValue(array())->end()
            ->scalarNode('get_elements_service')->defaultNull()->end()
            ->end()
            ->end()
            ->end()
            ->arrayNode('onclick')
            ->prototype('array')
            ->children()
            ->scalarNode('label')->end()
            ->scalarNode('name')->end()
            ->end()
            ->end()
            ->end()
            ->end();

        return $treeBuilder;
    }
}
