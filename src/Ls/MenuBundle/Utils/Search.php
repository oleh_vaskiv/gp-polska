<?php

namespace Ls\MenuBundle\Utils;

use Symfony\Component\DependencyInjection\ContainerInterface;

class Search {

    private $container;
    private $method;

    public function __construct(ContainerInterface $container, $method = '') {
        $this->container = $container;
        $this->method = $method;
    }

    public function search($search = '') {
        switch ($this->method) {
            case 'oferta' :
                return $this->oferta();

            default:
                break;
        }

        return array();
    }

    public function oferta($search = '') {

        $em = $this->container->get('doctrine')->getManager();

        $search = mb_strtolower($search);

        $items = $em->createQueryBuilder()
                ->select('s')
                ->from('LsOfferBundle:Offer', 's')
                ->where('s.title LIKE :search')
                ->orderBy('s.title', 'ASC')
                ->setParameter('search', '%' . $search . '%')
                ->getQuery()
                ->getResult();

        $ret_arr = array();
        foreach ($items as $item) {
            $ret_arr[] = array(
                'parameters' => array('slug' => $item->getSlug()),
                'title' => $item->getTitle()
            );
        }

        return $ret_arr;
    }
}
