<?php

namespace Ls\ContactBundle\Form;

use Ls\CoreBundle\Form\GoogleRecaptchaType;
use Ls\CoreBundle\Validator\Constraints\GoogleRecaptcha;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;

class ContactType extends AbstractType {
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder->add('name', null, array(
                'label' => 'Imię i nazwisko',
                'required' => true,
                'attr' => array('placeholder' => 'Imię i nazwisko'),
                'constraints' => array(
                    new NotBlank(array('message' => 'Wypełnij pole'))
                )
            )
        );
        
        $builder->add('phone', null, array(
                'label' => 'Numer telefonu',
                'attr' => array('placeholder' => 'Numer telefonu'),
            )
        );
        
        $builder->add('email', null, array(
                'label' => 'Adres e-mail',
                'attr' => array('placeholder' => 'Adres e-mail'),
                'required' => true,
                'constraints' => array(
                    new Email(array(
                        'message' => 'Podaj poprawny adres e-mail'
                    ))
                )
            )
        );
        $builder->add('content', TextareaType::class, array(
                'label' => 'Treść wiadomości',
                'attr' => array('placeholder' => 'Treść wiadomości'),
                'required' => true,
                'constraints' => array(
                    new NotBlank(array('message' => 'Wypełnij pole'))
                )
            )
        );
//        $builder->add('google_recaptcha', GoogleRecaptchaType::class, array(
//                'label' => 'Ochrona antyspamowa',
//                'error_bubbling' => false,
//                'mapped' => false,
//                'constraints' => array(
//                    new GoogleRecaptcha()
//                )
//            )
//        );
        $builder->add('submit', SubmitType::class, array(
                'label' => 'Wyślij'
            )
        );
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array());
    }

    /**
     * @return string
     */
    public function getBlockPrefix() {
        return 'form_contact';
    }
}
