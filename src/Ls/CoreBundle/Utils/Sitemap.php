<?php

namespace Ls\CoreBundle\Utils;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class Sitemap
{
    private $container;
    private $router;
    private $xml;
    private $em;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->em = $this->container->get('doctrine')->getManager();
        $this->router = $this->container->get('router');
        $this->xml = null;
    }

    public function generate()
    {
        $this->xml = new \SimpleXMLElement("<?xml version='1.0' encoding='UTF-8' standalone='yes'?><urlset xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\"></urlset>");

        $url = $this->container->get('router')->generate('ls_core_homepage', array(), UrlGeneratorInterface::ABSOLUTE_URL);
        $child = $this->xml->addChild('url');
        $child->addChild('loc', $url);

        $count = $this->em->createQueryBuilder()
            ->select('COUNT(c.id)')
            ->from('LsMenuBundle:MenuItem', 'c')
            ->where('c.route = :route')
            ->setParameter('route', 'ls_contact')
            ->getQuery()
            ->getSingleScalarResult();

        if ($count > 0) {
            $url = $this->container->get('router')->generate('ls_contact', array(), UrlGeneratorInterface::ABSOLUTE_URL);
            $child = $this->xml->addChild('url');
            $child->addChild('loc', $url);
        }

        if (class_exists('\\Ls\\NewsBundle\\Entity\\News')) {
            $this->aktualnosci();
        }

        $filename = __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'web' . DIRECTORY_SEPARATOR . 'sitemap.xml';

        $dom = new \DOMDocument('1.0');
        $dom->preserveWhiteSpace = false;
        $dom->formatOutput = true;
        $dom->loadXML($this->xml->asXML());
        $dom->save($filename);
    }

    public function aktualnosci()
    {
        $count = $this->em->createQueryBuilder()
            ->select('COUNT(c.id)')
            ->from('LsMenuBundle:MenuItem', 'c')
            ->where('c.route = :route')
            ->setParameter('route', 'ls_offer')
            ->getQuery()
            ->getSingleScalarResult();

        if ($count > 0) {
            $items = $this->em->createQueryBuilder()
                ->select('e')
                ->from('LsOfferBundle:Offer', 'e')
                ->orderBy('e.published_at', 'ASC')
                ->getQuery()
                ->getResult();

            foreach ($items as $item) {
                $url = $this->container->get('router')->generate('ls_offer_show', array('slug' => $item->getSlug()), UrlGeneratorInterface::ABSOLUTE_URL);
                $child = $this->xml->addChild('url');
                $child->addChild('loc', $url);
                if ($item->getUpdatedAt() != null) {
                    $child->addChild('lastmod', $item->getUpdatedAt()->format(\DateTime::ATOM));
                }
            }
        }
    }
}
