// Waliduje formularz kontaktowy
function sendContact(path, send_data) {
    $('div[id^="qtip-"]').each(function () {
        _qtip2 = $(this).data("qtip");
        if (_qtip2 != undefined) {
            _qtip2.destroy(true);
        }
    });

    var send = true;
    var data = {
        name: $('#form_contact_name').val(),
        email: $('#form_contact_email').val(),
        phone: $('#form_contact_phone').val(),
        message: $('#form_contact_content').val()
    };

    if (data.name.length == 0) {
        send = false;
        showError('#form_contact_name', 'Wypełnij pole.')
    }

    if (data.email.length == 0 && data.phone.length == 0) {
        send = false;
        showError('#form_contact_email', 'Podaj adres e-mail lub numeru telefonu.')
        showError('#form_contact_phone', 'Podaj adres e-mail lub numeru telefonu.')
    } else if (data.email.length != 0) {
        if (!checkEmail(data.email)) {
            send = false;
            showError('#form_contact_email', 'Podaj poprawny email.')
        }
    }

    if (data.message.length == 0) {
        send = false;
        showError('#form_contact_content', 'Wypełnij pole.')
    }

    if (send) {
        $.ajax({
            type: 'post',
            url: path,
            data: send_data,
            success: function (response) {
                if (response === 'OK') {
                    $('#form_contact_name').val('');
                    $('#form_contact_email').val('');
                    $('#form_contact_phone').val('');
                    $('#form_contact_content').val('');
                    
                        $('.message').show().html('<p class="green-color">Twoja wiadomość została wysłana.</p>')
                        $('.message').delay(7000).fadeOut();
                } else {
                    $('.message').show().html('<p class="red-color">Wystąpił błąd podczas wysyłania wiadomości.</p>')
                    $('.message').delay(7000).fadeOut();
                }
            }
        });
    }

    return false;
}


// Sprawdza poprawnoĹÄ adresu email
function checkEmail(emailAddress) {
    var re = /^[a-zA-Z0-9._\-]+@[a-zA-Z0-9.\-]+\.[a-zA-Z]{2,6}$/;
    if (re.test(emailAddress)) {
        return true;
    }

    return false;
}

function checkNum(val) {
    var re = /^\d+$/;
    if (re.test(val)) {
        return true;
    }

    return false;
}

// WyĹwietla bĹÄdy z walidacji.
function showError(field, error) {
    $(field).qtip({
        content: {
            title: null,
            text: error
        },
        position: {
            my: 'bottom left',
            at: 'top right',
            adjust: {
                x: -30
            }
        },
        show: {
            ready: true,
            event: false
        },
        hide: {
            event: 'click focus unfocus'
        },
        style: {
            classes: 'qtip-red qtip-rounded qtip-shadow'
        }
    });
}