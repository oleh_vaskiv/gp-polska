$(document).on('click', '.mobile-menu-btn', function () {
    if (!$('.menu-mobile').is(':visible')) {
        $('.menu-mobile').fadeIn(250);
    }
});

$(document).on('click', '.hide-menu', function () {
    if ($('.menu-mobile').is(':visible')) {
        $('.menu-mobile').fadeOut(250);
    }
});

//mobile selector
$(document).on('click', '.mobile-select .items .active-item', function () {
    $(this).parent().toggleClass('active');
});


$('.bxslider').bxSlider({
    auto: true,
    speed: 1000,
    controls: false,
    mode: 'fade',
    onSliderResize: reloadBX,
    onSliderLoad: function () {
        $(".bxslider").css("visibility", "visible");
    }
});

var gallery = $('.gallery-list').bxSlider({
    auto: false,
    controls: true,
    pager: false,
    slideSelector: '.img-border',
    minSlides: 1,
    maxSlides: 4,
    slideWidth: 204,
    infiniteLoop: false,
    hideControlOnEnd: true,
    slideMargin: 30,
    onSliderResize: reloadBX,
    onSliderLoad: function () {
        $(".gallery-list").css("visibility", "visible");
    }
});

$( "#form_contact" ).submit(function( event ) {
    event.preventDefault();
    sendContact($(this).attr('action'), $(this).serialize())
});


//animate move on click menu
$('.nav').on('click', 'a', function (event) {
    event.preventDefault();
    var link = $(this).attr('href');
    
    if (checkLink(link)) {
        $('.menu-mobile').fadeOut(250);
        animateMenu(link);
    } else {
        if (link.length > 1) {
            window.location.replace(link);
        }
    }
});

$('#slider').on('click', '.slider-link', function (event) {
    event.preventDefault();
    var link = $(this).attr('href');
    
    if (checkLink(link)) {
        animateMenu(link);
    } else {
        if (link.length > 1) {
            window.location.replace(link);
        }
    }
});

function checkLink(val) {
    var re = /#(.*)/;
    if (re.test(val) && val.length > 1) {
        return true;
    }

    return false;
}

function animateMenu(id) {
    var body = $("html, body");
    
    var top = $(id).position().top;

    body.stop(true, true).animate({scrollTop: top}, 1000, 'swing');
}

/*
Resize Callback 
*/ // Stores the current slide index.
function reloadBX(idx) {
  localStorage.cache = idx;
  // Reloads slider, 
  ///goes back to the slide it was on before resize,
  ///removes the stored index.
  function inner(idx) {
    setTimeout(slider.reloadSlider, 0);
    var current = parseInt(idx, 10);
    slider.goToSlide(current);
    slider.startAuto();
    localStorage.removeItem("cache");
  }
}