<?php

namespace Ls\OfferBundle\Controller;

use Doctrine\DBAL\Types\Type;
use Ls\CoreBundle\Utils\Tools;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Offer controller.
 *
 */
class FrontController extends Controller {

    /**
     * Lists all Offer entities on index page.
     *
     */
    public function sectionAction() {
        $em = $this->getDoctrine()->getManager();

        $today = new \DateTime();

        $qb = $em->createQueryBuilder();
        $entities = $qb->select('a')
            ->from('LsOfferBundle:Offer', 'a')
            ->where($qb->expr()->isNotNull('a.published_at'))
            ->andWhere('a.published_at <= :today')
            ->orderBy('a.published_at', 'DESC')
            ->setParameter('today', $today, Type::DATETIME)
            ->getQuery()
            ->getResult();

        return $this->render('LsOfferBundle:Front:section.html.twig', array(
            'entities' => $entities,
        ));
    }

    /**
     * Finds and displays a Offer entity.
     *
     */
    public function showAction($slug) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->createQueryBuilder()
            ->select('p')
            ->from('LsOfferBundle:Offer', 'p')
            ->where('p.slug = :slug')
            ->setParameter('slug', $slug)
            ->getQuery()
            ->getOneOrNullResult();

        if (null === $entity) {
            throw $this->createNotFoundException('Unable to find Offer entity.');
        }
        
        $today = new \DateTime();
        $qb = $em->createQueryBuilder();
        $other = $qb->select('a')
            ->from('LsOfferBundle:Offer', 'a')
            ->where($qb->expr()->isNotNull('a.published_at'))
            ->andWhere('a.published_at <= :today')
            ->andWhere('a.slug <> :slug')
            ->setParameter('slug', $slug)
            ->orderBy('a.published_at', 'ASC')
            ->setParameter('today', $today, Type::DATETIME)
            ->getQuery()
            ->getResult();

        return $this->render('LsOfferBundle:Front:show.html.twig', array(
            'entity' => $entity,
            'other' => $other,
        ));
    }
}
