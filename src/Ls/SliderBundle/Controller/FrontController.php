<?php

namespace Ls\SliderBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * News controller.
 *
 */
class FrontController extends Controller {

    public function sectionAction() {
        $em = $this->getDoctrine()->getManager();

        $qb = $em->createQueryBuilder();
        $entities = $qb->select('a')
            ->from('LsSliderBundle:SliderPhoto', 'a')
            ->getQuery()
            ->getResult();


        return $this->render('LsSliderBundle:Front:section.html.twig', array(
            'entities' => $entities,
        ));
    }
}
