<?php

namespace Ls\SliderBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Image;

class SliderPhotoType extends AbstractType {
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {
            $object = $event->getData();
            $form = $event->getForm();
            $size = $object->getThumbSize('full');

            $form->add('file', FileType::class, array(
                'label' => 'Nowe zdjęcie',
                'constraints' => array(
                    new Image(array(
                        'minWidth' => $size['width'],
                        'minHeight' => $size['height'],
                        'minWidthMessage' => 'Szerokość zdjęcie musi być większa niż ' . $size['width'] . 'px',
                        'minHeightMessage' => 'Wysokość zdjęcie musi być większa niż ' . $size['height'] . 'px',
                    ))
                )
            ));
        });
        
        $builder->add('title', null, array(
            'label' => 'Tytuł'
        ));
        
        $builder->add('content', null, array(
            'label' => 'Krótka treść'
        ));
        
        $builder->add('linkTitle', null, array(
            'label' => 'Nazwa przycisku'
        ));
        
        $builder->add('link', null, array(
            'label' => 'URL Adres'
        ));
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Ls\SliderBundle\Entity\SliderPhoto',
        ));
    }

    /**
     * @return string
     */
    public function getBlockPrefix() {
        return 'form_admin_slider_photo';
    }
}
