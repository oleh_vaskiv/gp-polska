<?php

namespace Ls\SectionBundle\Form;

use Ls\GalleryBundle\Form\HasGalleryType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Image;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Form\AbstractType;

class SectionType extends AbstractType {
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        parent::buildForm($builder, $options);
        
        $builder->add('section_title', null, array(
            'label' => 'Nazwa sekcji',
            'constraints' => array(
                new NotBlank(array(
                    'message' => 'Wypełnij pole'
                ))
            )
        ));
        
        $builder->add('title', null, array(
            'label' => 'Tytuł',
            'constraints' => array(
                new NotBlank(array(
                    'message' => 'Wypełnij pole'
                ))
            )
        ));

        $builder->add('content', null, array(
            'label' => 'Treść'
        ));

        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {
            $object = $event->getData();
            $form = $event->getForm();
            $size = $object->getThumbSize('detail');

            if (!$object || null === $object->getId()) {
                $form->add('file', FileType::class, array(
                    'label' => 'Nowe zdjęcie',
                    'constraints' => array(
                        new NotBlank(array(
                            'message' => 'Wybierz zdjęcie'
                        )),
                        new Image(array(
                            'minWidth' => $size['width'],
                            'minHeight' => $size['height'],
                            'minWidthMessage' => 'Szerokość zdjęcie musi być większa niż ' . $size['width'] . 'px',
                            'minHeightMessage' => 'Wysokość zdjęcie musi być większa niż ' . $size['height'] . 'px',
                        ))
                    )
                ));
            } else {
                $form->add('file', FileType::class, array(
                    'label' => 'Nowe zdjęcie',
                    'constraints' => array(
                        new Image(array(
                            'minWidth' => $size['width'],
                            'minHeight' => $size['height'],
                            'minWidthMessage' => 'Szerokość zdjęcie musi być większa niż ' . $size['width'] . 'px',
                            'minHeightMessage' => 'Wysokość zdjęcie musi być większa niż ' . $size['height'] . 'px',
                        ))
                    )
                ));
            }
        });
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Ls\SectionBundle\Entity\Section',
        ));
    }

    /**
     * @return string
     */
    public function getBlockPrefix() {
        return 'form_admin_section';
    }
}
