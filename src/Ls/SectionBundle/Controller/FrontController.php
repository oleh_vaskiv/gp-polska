<?php

namespace Ls\SectionBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Section controller.
 *
 */
class FrontController extends Controller {

    /**
     * Lists Section entity on index page.
     *
     */
    public function sectionAction($id) {
        $em = $this->getDoctrine()->getManager();


        $qb = $em->createQueryBuilder();
        $entity = $qb->select('a')
            ->from('LsSectionBundle:Section', 'a')
            ->where('a.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getOneOrNullResult();

        return $this->render('LsSectionBundle:Front:section.html.twig', array(
            'entity' => $entity,
        ));
    }

}
