<?php

namespace Ls\SectionBundle\Entity;

use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping as ORM;
use Ls\CoreBundle\Utils\Tools;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Section
 * @ORM\Table(name="section")
 * @ORM\Entity
 */
class Section
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @var integer
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @var string
     */
    private $section_title;
    
    /**
     * @ORM\Column(type="string", length=255)
     * @var string
     */
    private $title;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @var string
     */
    private $content;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $photo;

    /**
     * @ORM\Column(type="datetime")
     * @var \DateTime
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @var \DateTime
     */
    private $updated_at;

    protected $file;

    protected $detailWidth = 234;
    protected $detailHeight = 246;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->created_at = new \DateTime();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
    
    /**
     * Set title
     *
     * @param string $title
     * @return Section
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set section_title
     *
     * @param string $section_title
     * @return Section
     */
    public function setSectionTitle($section_title)
    {
        $this->section_title = $section_title;

        return $this;
    }

    /**
     * Get section_title
     *
     * @return string
     */
    public function getSectionTitle()
    {
        return $this->section_title;
    }

    /**
     * Set content
     *
     * @param string $content
     * @return Section
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set photo
     *
     * @param string $photo
     * @return Section
     */
    public function setPhoto($photo)
    {
        $this->photo = $photo;

        return $this;
    }

    /**
     * Get photo
     *
     * @return string
     */
    public function getPhoto()
    {
        return $this->photo;
    }

    /**
     * Set created_at
     *
     * @param \DateTime $created_at
     * @return Section
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;

        return $this;
    }

    /**
     * Get created_at
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set updated_at
     *
     * @param \DateTime $updated_at
     * @return Section
     */
    public function setUpdatedAt($updated_at)
    {
        $this->updated_at = $updated_at;

        return $this;
    }

    /**
     * Get updated_at
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    public function __toString()
    {
        if (is_null($this->getTitle())) {
            return 'NULL';
        }

        return $this->getTitle();
    }

    public function getThumbSize($type)
    {
        $size = array();
        switch ($type) {
            case 'detail':
                $size['width'] = $this->detailWidth;
                $size['height'] = $this->detailHeight;
                break;
        }

        return $size;
    }

    public function getThumbWebPath($type)
    {
        if (empty($this->photo)) {
            return null;
        } else {
            $sThumbName = '';
            switch ($type) {
                case 'detail':
                    $sThumbName = Tools::thumbName($this->photo, '_d');
                    break;
            }

            return '/' . $this->getUploadDir() . '/' . $sThumbName;
        }
    }

    public function getThumbAbsolutePath($type)
    {
        if (empty($this->photo)) {
            return null;
        } else {
            $sThumbName = '';
            switch ($type) {
                case 'detail':
                    $sThumbName = Tools::thumbName($this->photo, '_d');
                    break;
            }

            return $this->getUploadRootDir() . DIRECTORY_SEPARATOR . $sThumbName;
        }
    }

    public function getPhotoSize()
    {
        $temp = getimagesize($this->getPhotoAbsolutePath());
        $size = array(
            'width'  => $temp[0],
            'height' => $temp[1],
        );

        return $size;
    }

    public function setFile(UploadedFile $file = null)
    {
        $this->file = $file;
    }

    public function getFile()
    {
        return $this->file;
    }

    public function deletePhoto()
    {
        if (!empty($this->photo)) {
            $filename = $this->getPhotoAbsolutePath();
            $filename_d = Tools::thumbName($filename, '_d');
            if (file_exists($filename)) {
                @unlink($filename);
            }
            if (file_exists($filename_d)) {
                @unlink($filename_d);
            }
        }
    }

    public function getPhotoAbsolutePath()
    {
        return empty($this->photo) ? null : $this->getUploadRootDir() . DIRECTORY_SEPARATOR . $this->photo;
    }

    public function getPhotoWebPath()
    {
        return empty($this->photo) ? null : '/' . $this->getUploadDir() . '/' . $this->photo;
    }

    protected function getUploadRootDir()
    {
        // the absolute directory path where uploaded documents should be saved
        return __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'web' . DIRECTORY_SEPARATOR . $this->getUploadDir();
    }

    protected function getUploadDir()
    {
        // get rid of the __DIR__ so it doesn't screw when displaying uploaded doc/image in the view.
        return 'upload/section';
    }

    public function upload()
    {
        if (null === $this->file) {
            return;
        }

        $sFileName = $this->getPhoto();

        $this->file->move($this->getUploadRootDir(), $sFileName);

        $this->createThumbs();

        unset($this->file);
    }

    public function createThumbs()
    {
        if (null !== $this->getPhotoAbsolutePath()) {
            $sFileName = $this->getPhoto();
            $sSourceName = $this->getUploadRootDir() . DIRECTORY_SEPARATOR . $sFileName;

            //tworzy wszystkie miniatury wycinajac ze srodka zdjecia
            $thumb = \PhpThumbFactory::create($sSourceName);
            $sThumbNameD = Tools::thumbName($sSourceName, '_d');
            $aThumbSizeD = $this->getThumbSize('detail');
            $thumb->adaptiveResize($aThumbSizeD['width'] + 2, $aThumbSizeD['height'] + 2);
            $thumb->crop(0, 0, $aThumbSizeD['width'], $aThumbSizeD['height']);
            $thumb->save($sThumbNameD);
        }
    }

    public function Thumb($x, $y, $x2, $y2, $type)
    {
        $sSourceName = $this->getUploadRootDir() . DIRECTORY_SEPARATOR . $this->getPhoto();
        $sThumbName = $this->getThumbAbsolutePath($type);
        $aThumbSize = $this->getThumbSize($type);
        $thumb = \PhpThumbFactory::create($sSourceName);

        $cropWidth = $x2 - $x;
        $cropHeight = $y2 - $y;

        $thumb->crop($x, $y, $cropWidth, $cropHeight);
        $thumb->resize($aThumbSize['width'] + 2, $aThumbSize['height'] + 2);
        $thumb->crop(0, 0, $aThumbSize['width'], $aThumbSize['height']);
        $thumb->save($sThumbName);
    }

}