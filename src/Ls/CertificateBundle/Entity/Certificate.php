<?php

namespace Ls\CertificateBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Ls\CoreBundle\Utils\Tools;

/**
 * Certificate
 * @ORM\Table(name="certificate")
 * @ORM\Entity
 */
class Certificate {

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @var integer
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @var string
     */
    private $filename;

    /**
     * @ORM\Column(type="integer")
     * @var integer
     */
    private $arrangement;

    /**
     * @Assert\File(maxSize="8388608")
     */
    protected $file;

    protected $fullWidth = 164;
    protected $fullHeight = 225;
    
    /**
     * Constructor
     */
    public function __construct() {
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set filename
     *
     * @param string $filename
     * @return Certificate
     */
    public function setFilename($filename) {
        $this->filename = $filename;

        return $this;
    }

    /**
     * Get filename
     *
     * @return string
     */
    public function getFilename() {
        return $this->filename;
    }
    
    /**
     * Set title
     *
     * @param string $title
     * @return Certificate
     */
    public function setTitle($title) {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle() {
        return $this->title;
    }
    
    /**
     * Set content
     *
     * @param string $content
     * @return Certificate
     */
    public function setContent($content) {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent() {
        return $this->content;
    }
    
    /**
     * Set linkTitle
     *
     * @param string $linkTitle
     * @return Certificate
     */
    public function setLinkTitle($linkTitle) {
        $this->linkTitle = $linkTitle;

        return $this;
    }

    /**
     * Get linkTitle
     *
     * @return string
     */
    public function getlinkTitle() {
        return $this->linkTitle;
    }
    
    /**
     * Set link
     *
     * @param string $link
     * @return Certificate
     */
    public function setLink($link) {
        $this->link = $link;

        return $this;
    }

    /**
     * Get link
     *
     * @return string
     */
    public function getlink() {
        return $this->link;
    }

    /**
     * Set arrangement
     *
     * @param integer $arrangement
     * @return Certificate
     */
    public function setArrangement($arrangement) {
        $this->arrangement = $arrangement;

        return $this;
    }

    /**
     * Get arrangement
     *
     * @return integer
     */
    public function getArrangement() {
        return $this->arrangement;
    }

    public function __toString() {
        if (is_null($this->getFilename())) {
            return 'NULL';
        }
        return $this->getFilename();
    }

    public function getThumbSize($type) {
        $size = array();
        switch ($type) {
            case 'full':
                $size['width'] = $this->fullWidth;
                $size['height'] = $this->fullHeight;
                break;
        }
        return $size;
    }

    public function getThumbWebPath($type) {
        if (empty($this->filename)) {
            return null;
        } else {
            $sThumbName = '';
            switch ($type) {
                case 'full':
                    $sThumbName = Tools::thumbName($this->filename, '_f');
                    break;
            }
            return '/' . $this->getUploadDir() . '/' . $sThumbName;
        }
    }

    public function getThumbAbsolutePath($type) {
        if (empty($this->filename)) {
            return null;
        } else {
            $sThumbName = '';
            switch ($type) {
                case 'full':
                    $sThumbName = Tools::thumbName($this->filename, '_f');
                    break;
            }
            return $this->getUploadRootDir() . DIRECTORY_SEPARATOR . $sThumbName;
        }
    }

    public function getPhotoSize() {
        $temp = getimagesize($this->getPhotoAbsolutePath());
        $size = array(
            'width' => $temp[0],
            'height' => $temp[1]
        );
        return $size;
    }

    public function setFile(UploadedFile $file = null) {
        $this->file = $file;
    }

    public function getFile() {
        return $this->file;
    }

    public function deletePhoto() {
        if (!empty($this->filename)) {
            $filename = $this->getPhotoAbsolutePath();
            $filename_f = Tools::thumbName($filename, '_f');
            if (file_exists($filename)) {
                @unlink($filename);
            }
            if (file_exists($filename_f)) {
                @unlink($filename_f);
            }
        }
    }

    public function getPhotoAbsolutePath() {
        return empty($this->filename) ? null : $this->getUploadRootDir() . DIRECTORY_SEPARATOR . $this->filename;
    }

    public function getPhotoWebPath() {
        return empty($this->filename) ? null : '/' . $this->getUploadDir() . '/' . $this->filename;
    }

    public function getUploadRootDir() {
        // the absolute directory path where uploaded documents should be saved
        return __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'web' . DIRECTORY_SEPARATOR . $this->getUploadDir();
    }

    public function getUploadDir() {
        // get rid of the __DIR__ so it doesn't screw when displaying uploaded doc/image in the view.
        return 'upload/certificate';
    }

    public function upload() {
        if (null === $this->file) {
            return;
        }

        $sFileName = $this->getFilename();

        $this->file->move($this->getUploadRootDir(), $sFileName);

        $this->createThumbs();

        unset($this->file);
    }

    public function createThumbs() {
        if (null !== $this->getPhotoAbsolutePath()) {
            $sFileName = $this->getFilename();
            $sSourceName = $this->getUploadRootDir() . DIRECTORY_SEPARATOR . $sFileName;

            //tworzy wszystkie miniatury wycinajac ze srodka zdjecia
            $thumb = \PhpThumbFactory::create($sSourceName);
            $sThumbNameF = Tools::thumbName($sSourceName, '_f');
            $aThumbSizeF = $this->getThumbSize('full');
            $thumb->adaptiveResize($aThumbSizeF['width'] + 2, $aThumbSizeF['height'] + 2);
            $thumb->crop(0, 0, $aThumbSizeF['width'], $aThumbSizeF['height']);
            $thumb->save($sThumbNameF);
        }
    }

    public function Thumb($x, $y, $x2, $y2, $type) {
        $sSourceName = $this->getUploadRootDir() . DIRECTORY_SEPARATOR . $this->getFilename();
        $sThumbName = $this->getThumbAbsolutePath($type);
        $aThumbSize = $this->getThumbSize($type);
        $thumb = \PhpThumbFactory::create($sSourceName);

        $cropWidth = $x2 - $x;
        $cropHeight = $y2 - $y;

        $thumb->crop($x, $y, $cropWidth, $cropHeight);
        $thumb->resize($aThumbSize['width'] + 2, $aThumbSize['height'] + 2);
        $thumb->crop(0, 0, $aThumbSize['width'], $aThumbSize['height']);
        $thumb->save($sThumbName);
    }
}