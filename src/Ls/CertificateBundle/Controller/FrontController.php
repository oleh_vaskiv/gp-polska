<?php

namespace Ls\CertificateBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * News controller.
 *
 */
class FrontController extends Controller {

    public function sectionAction() {
        $em = $this->getDoctrine()->getManager();

        $qb = $em->createQueryBuilder();
        $entities = $qb->select('a')
            ->from('LsCertificateBundle:Certificate', 'a')
            ->getQuery()
            ->getResult();


        return $this->render('LsCertificateBundle:Front:section.html.twig', array(
            'entities' => $entities,
        ));
    }
}
