-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Czas generowania: 25 Lut 2019, 18:28
-- Wersja serwera: 5.7.24
-- Wersja PHP: 7.2.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `gppolska`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `certificate`
--

DROP TABLE IF EXISTS `certificate`;
CREATE TABLE IF NOT EXISTS `certificate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `filename` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `arrangement` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Zrzut danych tabeli `certificate`
--

INSERT INTO `certificate` (`id`, `filename`, `arrangement`) VALUES
(1, 'certificate-image-58c6e339af467.png', 1),
(2, 'certificate-image-58c6e339ee042.png', 2),
(3, 'certificate-image-58c6e33a31539.jpg', 3),
(4, 'certificate-image-58c6e34f20770.png', 4),
(9, 'certificate-image-58c7d23ee7f36.png', 5);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `contact`
--

DROP TABLE IF EXISTS `contact`;
CREATE TABLE IF NOT EXISTS `contact` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` datetime NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Zrzut danych tabeli `contact`
--

INSERT INTO `contact` (`id`, `name`, `email`, `content`, `created_at`, `phone`) VALUES
(1, 'fdgfdgdfg', 'fdgdgfg@fghhf.ru', 'dfgfdgdfg', '2017-03-14 11:54:40', NULL),
(2, 'fhfghfgh', 'vaskiv7@gmail.com', 'fghfghfhfgh', '2017-03-14 11:55:22', '+380951146787'),
(3, 'ghfghgfh', 'vaskiv7@gmail.com', 'fsdfsfdsdfsf', '2017-03-14 12:56:44', '+380951146787'),
(4, 'gfhfhfgh', 'gfh@fdgdfg.ghfgh', 'fghfhfhfgh', '2017-03-14 12:57:25', NULL),
(5, 'Oleh Vaskiv', 'vaskiv7@gmail.com', 'dfgdfgfdgdfgdfg', '2017-03-14 14:40:12', '+380951146787'),
(6, 'Oleh Vaskiv', NULL, 'fdgfdggdfg', '2017-03-14 15:02:08', '+380951146787'),
(7, 'Oleh Vaskiv', 'vaskiv7@gmail.com', 'fgdgdgdfg', '2017-03-15 10:09:37', '+380951146787');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `gallery`
--

DROP TABLE IF EXISTS `gallery`;
CREATE TABLE IF NOT EXISTS `gallery` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `old_slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `on_list` tinyint(1) NOT NULL,
  `attachable` tinyint(1) NOT NULL,
  `seo_generate` tinyint(1) DEFAULT NULL,
  `seo_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_keywords` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Zrzut danych tabeli `gallery`
--

INSERT INTO `gallery` (`id`, `title`, `slug`, `old_slug`, `on_list`, `attachable`, `seo_generate`, `seo_title`, `seo_keywords`, `seo_description`, `created_at`, `updated_at`) VALUES
(1, 'Oferta - Folie', 'oferta-folie', 'oferta-folie', 1, 1, 0, 'Oferta - Folie', NULL, NULL, '2017-03-14 14:22:50', '2017-03-14 14:22:50');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `gallery_photo`
--

DROP TABLE IF EXISTS `gallery_photo`;
CREATE TABLE IF NOT EXISTS `gallery_photo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gallery_id` int(11) DEFAULT NULL,
  `photo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `arrangement` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_F02A543B4E7AF8F` (`gallery_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Zrzut danych tabeli `gallery_photo`
--

INSERT INTO `gallery_photo` (`id`, `gallery_id`, `photo`, `arrangement`) VALUES
(1, 1, 'gallery-image-58c7fcd870a65.png', 1),
(2, 1, 'gallery-image-58c7fcd8b7b21.png', 2),
(3, 1, 'gallery-image-58c7fcd8c1b02.png', 3),
(4, 1, 'gallery-image-58c7fce24092d.png', 4),
(5, 1, 'gallery-image-58c7fce252772.png', 5);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `menu_item`
--

DROP TABLE IF EXISTS `menu_item`;
CREATE TABLE IF NOT EXISTS `menu_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT NULL,
  `location` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `route` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `route_parameters` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `onclick` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `blank` tinyint(1) DEFAULT NULL,
  `arrangement` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_D754D550727ACA70` (`parent_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Zrzut danych tabeli `menu_item`
--

INSERT INTO `menu_item` (`id`, `parent_id`, `location`, `type`, `title`, `route`, `route_parameters`, `url`, `onclick`, `blank`, `arrangement`) VALUES
(1, NULL, 'menu_top', 'url', 'O firmie', NULL, NULL, '#o-firmie', NULL, 0, 1),
(2, NULL, 'menu_top', 'url', 'Oferta', NULL, NULL, '#', NULL, 0, 2),
(3, NULL, 'menu_top', 'url', 'Gwarancja i certyfikaty', NULL, NULL, '#certyfikaty', NULL, 0, 3),
(4, NULL, 'menu_top', 'url', 'Kontakt', NULL, NULL, '#kontakt', NULL, 0, 4),
(5, 2, 'menu_top', 'route', 'Folie', 'ls_offer_show', '{\"slug\":\"folie-1\"}', '//', NULL, 0, 1),
(6, 2, 'menu_top', 'route', 'Opakowania', 'ls_offer_show', '{\"slug\":\"opakowania\"}', '/', NULL, 0, 2),
(7, 2, 'menu_top', 'route', 'Granulat', 'ls_offer_show', '{\"slug\":\"granulat\"}', '/', NULL, 0, 3);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `news`
--

DROP TABLE IF EXISTS `news`;
CREATE TABLE IF NOT EXISTS `news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gallery_id` int(11) DEFAULT NULL,
  `show_main` tinyint(1) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `old_slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content_short_generate` tinyint(1) DEFAULT NULL,
  `content_short` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci,
  `photo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_generate` tinyint(1) DEFAULT NULL,
  `seo_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_keywords` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `published_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_1DD399504E7AF8F` (`gallery_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `offer`
--

DROP TABLE IF EXISTS `offer`;
CREATE TABLE IF NOT EXISTS `offer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gallery_id` int(11) DEFAULT NULL,
  `show_main` tinyint(1) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `old_slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci,
  `photo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_generate` tinyint(1) DEFAULT NULL,
  `seo_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_keywords` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `published_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_29D6873E4E7AF8F` (`gallery_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Zrzut danych tabeli `offer`
--

INSERT INTO `offer` (`id`, `gallery_id`, `show_main`, `title`, `slug`, `old_slug`, `content`, `photo`, `seo_generate`, `seo_title`, `seo_keywords`, `seo_description`, `created_at`, `updated_at`, `published_at`) VALUES
(2, 1, NULL, 'Folie', 'folie-1', 'folie-1', '<h2 class=\"h2\">\r\n	Folia maszynowa&nbsp;</h2>\r\n<p>\r\n	stosowana jest do pakowania i zabezpieczania ładunk&oacute;w za pośrednictwem specjalnie zaprojektowanych do tego urządzeń. Odpowiednie, w pełni automatyczne (bądź p&oacute;ł-automatyczne) maszyny wyposażone są w r&oacute;żnego rodzaju systemy rozciągania, dzięki czemu wszyscy użytkownicy maszynowych folii stretch mają pewność prawidłowego zabezpieczenia ładunku. Nie jest ona folią przeznaczoną do pakowania ręcznego.</p>\r\n<div>\r\n	<h2 class=\"h2\">\r\n		Folia maszynowa stretch</h2>\r\n	<p>\r\n		dostępna jest w r&oacute;żnych opcjach, w zależności od potrzeb klienta, tj. waga rolki &nbsp;16-18 kg lub 13-14kg.</p>\r\n	<p>\r\n		Najważniejszą cechą tego typu folii jest ochrona towaru przed wilgocią, pyłem, brudem i kurzem; stabilizacja ładunk&oacute;w paletowych oraz zamykanie i uszczelnianie karton&oacute;w, co powoduje wzrost jej popularności z miesiąca na miesiąc. Decyduje o tym r&oacute;wnież przywoływany już czynnik ekonomiczny, bowiem w zestawieniu z materiałami o podobnych właściwościach i parametrach, folia wypada niepor&oacute;wnywalnie korzystniej.</p>\r\n	<p>\r\n		Folia maszynowa stretch wykorzystywana jest najczęściej przez zakłady produkcyjne oraz firmy dystrybucyjne, w kt&oacute;rych przyśpiesza się proces pakowania.</p>\r\n</div>\r\n<p>\r\n	&nbsp;</p>', 'offer-image-58c6acba6cbd2.png', 0, 'Folie', 'Folie, Folia, maszynowa, stosowana, jest, pakowania, zabezpieczania, ładunków, pośrednictwem, specjalnie', 'Folia maszynowa stosowana jest do pakowania i zabezpieczania ładunków za pośrednictwem specjalnie zaprojektowanych do tego urządzeń. Odpowiednie, w pełni...', '2017-03-13 14:29:14', '2017-03-14 14:24:06', '2017-03-11 14:22:00'),
(3, 1, NULL, 'Opakowania', 'opakowania', 'opakowania', '<h2 class=\"h2\">\r\n	Folia maszynowa&nbsp;</h2>\r\n<p>\r\n	stosowana jest do pakowania i zabezpieczania ładunk&oacute;w za pośrednictwem specjalnie zaprojektowanych do tego urządzeń. Odpowiednie, w pełni automatyczne (bądź p&oacute;ł-automatyczne) maszyny wyposażone są w r&oacute;żnego rodzaju systemy rozciągania, dzięki czemu wszyscy użytkownicy maszynowych folii stretch mają pewność prawidłowego zabezpieczenia ładunku. Nie jest ona folią przeznaczoną do pakowania ręcznego.</p>\r\n<div>\r\n	<h2 class=\"h2\">\r\n		Folia maszynowa stretch</h2>\r\n	<p>\r\n		dostępna jest w r&oacute;żnych opcjach, w zależności od potrzeb klienta, tj. waga rolki &nbsp;16-18 kg lub 13-14kg.</p>\r\n	<p>\r\n		Najważniejszą cechą tego typu folii jest ochrona towaru przed wilgocią, pyłem, brudem i kurzem; stabilizacja ładunk&oacute;w paletowych oraz zamykanie i uszczelnianie karton&oacute;w, co powoduje wzrost jej popularności z miesiąca na miesiąc. Decyduje o tym r&oacute;wnież przywoływany już czynnik ekonomiczny, bowiem w zestawieniu z materiałami o podobnych właściwościach i parametrach, folia wypada niepor&oacute;wnywalnie korzystniej.</p>\r\n	<p>\r\n		Folia maszynowa stretch wykorzystywana jest najczęściej przez zakłady produkcyjne oraz firmy dystrybucyjne, w kt&oacute;rych przyśpiesza się proces pakowania.</p>\r\n</div>\r\n<div>\r\n	&nbsp;</div>', 'offer-image-58c6aceced2e0.png', 0, 'Opakowania', 'Opakowania, Folia, maszynowa, stosowana, jest, pakowania, zabezpieczania, ładunków, pośrednictwem, specjalnie', 'Folia maszynowa stosowana jest do pakowania i zabezpieczania ładunków za pośrednictwem specjalnie zaprojektowanych do tego urządzeń. Odpowiednie, w pełni...', '2017-03-13 14:30:04', '2017-03-14 14:38:22', '2017-03-12 14:29:00'),
(4, 1, NULL, 'Granulat', 'granulat', 'granulat', '<h2 class=\"h2\">\r\n	Folia maszynowa&nbsp;</h2>\r\n<p>\r\n	stosowana jest do pakowania i zabezpieczania ładunk&oacute;w za pośrednictwem specjalnie zaprojektowanych do tego urządzeń. Odpowiednie, w pełni automatyczne (bądź p&oacute;ł-automatyczne) maszyny wyposażone są w r&oacute;żnego rodzaju systemy rozciągania, dzięki czemu wszyscy użytkownicy maszynowych folii stretch mają pewność prawidłowego zabezpieczenia ładunku. Nie jest ona folią przeznaczoną do pakowania ręcznego.</p>\r\n<div>\r\n	<h2 class=\"h2\">\r\n		Folia maszynowa stretch</h2>\r\n	<p>\r\n		dostępna jest w r&oacute;żnych opcjach, w zależności od potrzeb klienta, tj. waga rolki &nbsp;16-18 kg lub 13-14kg.</p>\r\n	<p>\r\n		Najważniejszą cechą tego typu folii jest ochrona towaru przed wilgocią, pyłem, brudem i kurzem; stabilizacja ładunk&oacute;w paletowych oraz zamykanie i uszczelnianie karton&oacute;w, co powoduje wzrost jej popularności z miesiąca na miesiąc. Decyduje o tym r&oacute;wnież przywoływany już czynnik ekonomiczny, bowiem w zestawieniu z materiałami o podobnych właściwościach i parametrach, folia wypada niepor&oacute;wnywalnie korzystniej.</p>\r\n	<p>\r\n		Folia maszynowa stretch wykorzystywana jest najczęściej przez zakłady produkcyjne oraz firmy dystrybucyjne, w kt&oacute;rych przyśpiesza się proces pakowania.</p>\r\n</div>\r\n<p>\r\n	&nbsp;</p>', 'offer-image-58c6ad19299ec.png', 0, 'Granulat', 'Granulat, Folia, maszynowa, stosowana, jest, pakowania, zabezpieczania, ładunków, pośrednictwem, specjalnie', 'Folia maszynowa stosowana jest do pakowania i zabezpieczania ładunków za pośrednictwem specjalnie zaprojektowanych do tego urządzeń. Odpowiednie, w pełni...', '2017-03-13 14:30:48', '2017-03-14 14:38:26', '2017-03-13 14:29:00');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `page`
--

DROP TABLE IF EXISTS `page`;
CREATE TABLE IF NOT EXISTS `page` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gallery_id` int(11) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `old_slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content_short_generate` tinyint(1) DEFAULT NULL,
  `content_short` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci,
  `seo_generate` tinyint(1) DEFAULT NULL,
  `seo_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_keywords` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_140AB6204E7AF8F` (`gallery_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `section`
--

DROP TABLE IF EXISTS `section`;
CREATE TABLE IF NOT EXISTS `section` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci,
  `photo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `section_title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Zrzut danych tabeli `section`
--

INSERT INTO `section` (`id`, `title`, `content`, `photo`, `created_at`, `updated_at`, `section_title`) VALUES
(1, 'Firma GP Polska  to wiodący producent  Folii Stretch  oraz Tulei Tekturowych.', '<div class=\"column\">\r\n	W produkcji folii stretch specjalizujemy się od 2010 roku. Dynamiczny rozw&oacute;j firmy zaowocował uruchomieniem w 2015 roku produkcji tulei tekturowych co uczyniło nas niezależnym producentem gotowych rolek folii stretch. Zakupiliśmy r&oacute;wnież najwyższej klasy maszyny do automatycznego przewijania folii stretch oraz maszyny.</div>\r\n<div class=\"column\">\r\n	Jako profesjonalny producent i dostawca produktu jakim jest folia stretch jumbo, oferujemy Państwu produkty o najwyższej jakości.</div>', 'section-image-58c6c9eb694a6.png', '2017-03-13 16:33:45', '2017-03-13 17:40:33', 'O firmie');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `setting`
--

DROP TABLE IF EXISTS `setting`;
CREATE TABLE IF NOT EXISTS `setting` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `value` longtext COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_9F74B898EA750E8` (`label`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Zrzut danych tabeli `setting`
--

INSERT INTO `setting` (`id`, `label`, `description`, `value`) VALUES
(1, 'seo_description', 'Domyślna wartość meta tagu \"description\"', 'Instalka CMS Lemonade Studio'),
(2, 'seo_keywords', 'Domyślna wartość meta tagu \"keywords\"', NULL),
(3, 'seo_title', 'Domyślna wartość meta tagu \"title\"', 'Instalka'),
(4, 'email_to_contact', 'Adres/Adresy e-mail (oddzielone przecinkiem) na które wysyłane są wiadomości z formularza kontaktowego', 'oleh@lemonadestudio.pl'),
(5, 'google_recaptcha_secret_key', 'Google reCAPTCHA - secret key', NULL),
(6, 'google_recaptcha_site_key', 'Google reCAPTCHA - site key', NULL),
(7, 'top_tagline', 'Tekst obok logo', 'PRODUCENT FOLII\r\noraz tuleii teksturowych'),
(8, 'certificate_content', 'Gwarancja i certyfikaty - Treść', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamcovoluptatem.'),
(9, 'contact_info', 'Kontakt - Informacja', 'GP Polska sp. z o. o.\r\n\r\ntel. +48 784 469 979\r\nbiuro@gp-polska.com\r\n\r\nul. Śniadeckich 28 lok. 4, \r\n60-774 Poznań'),
(10, 'second_contact_info', 'Kontakt - Dodatkowa informacja', 'Oddział Rzeszów\r\nKraczkowa 1581, 37-124 Kraczkowa\r\n\r\nMagazyn główny\r\nul. Wiosenna 37, 41-253 Czeladź'),
(11, 'copyright', 'Copyright', 'Wszelkie prawa zastrzeżone przez GP Polska.');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `slider_photo`
--

DROP TABLE IF EXISTS `slider_photo`;
CREATE TABLE IF NOT EXISTS `slider_photo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `filename` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `arrangement` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Zrzut danych tabeli `slider_photo`
--

INSERT INTO `slider_photo` (`id`, `filename`, `title`, `content`, `link_title`, `link`, `arrangement`) VALUES
(1, 'slider-image-58c2c7aea1be4.png', 'Dbamy  o jakość naszych produktów  i środowisko naturalne Dbamy  o jakość naszych produktów  i środowisko naturalne', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt... Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt...', 'Skontaktuj się', '#kontakt', 1),
(2, 'slider-image-58c2c7b3a4928.png', 'Dbamy  o jakość naszych produktów  i środowisko naturalne - 2', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt...', NULL, NULL, 2),
(3, 'slider-image-58c2c7b93e290.png', 'Dbamy  o jakość naszych produktów  i środowisko naturalne - 3', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt...', 'Folie', 'http://gppolska.loc/app_dev.php/oferta/folie-1', 3),
(4, 'slider-image-58c2c7be81669.png', 'Dbamy  o jakość naszych produktów  i środowisko naturalne - 4', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt...', 'Opakowanie', 'http://gppolska.loc/app_dev.php/oferta/opakowania', 4);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `salt` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` tinyint(1) NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `roles` longtext COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '(DC2Type:json_array)',
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_8D93D649F85E0677` (`username`),
  UNIQUE KEY `UNIQ_8D93D649E7927C74` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Zrzut danych tabeli `user`
--

INSERT INTO `user` (`id`, `username`, `email`, `salt`, `password`, `active`, `last_login`, `roles`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'biuro@lemonadestudio.pl', 'e40e0bc6f3ebacbed6e0e19b772845a8', 'Rd/3Txoa82/D9hi5AUbzSaMo0mR7YPs5tVG7TwD12SREfgob9BnzCItP1AhQxf7gi/IHE67DdDYPbMWPZIAROg==', 1, NULL, '[\"ROLE_ADMIN\",\"ROLE_ALLOWED_TO_SWITCH\",\"ROLE_USER\"]', '2015-07-31 13:46:34', NULL);

--
-- Ograniczenia dla zrzutów tabel
--

--
-- Ograniczenia dla tabeli `gallery_photo`
--
ALTER TABLE `gallery_photo`
  ADD CONSTRAINT `FK_F02A543B4E7AF8F` FOREIGN KEY (`gallery_id`) REFERENCES `gallery` (`id`) ON DELETE CASCADE;

--
-- Ograniczenia dla tabeli `menu_item`
--
ALTER TABLE `menu_item`
  ADD CONSTRAINT `FK_D754D550727ACA70` FOREIGN KEY (`parent_id`) REFERENCES `menu_item` (`id`);

--
-- Ograniczenia dla tabeli `news`
--
ALTER TABLE `news`
  ADD CONSTRAINT `FK_1DD399504E7AF8F` FOREIGN KEY (`gallery_id`) REFERENCES `gallery` (`id`);

--
-- Ograniczenia dla tabeli `offer`
--
ALTER TABLE `offer`
  ADD CONSTRAINT `FK_29D6873E4E7AF8F` FOREIGN KEY (`gallery_id`) REFERENCES `gallery` (`id`);

--
-- Ograniczenia dla tabeli `page`
--
ALTER TABLE `page`
  ADD CONSTRAINT `FK_140AB6204E7AF8F` FOREIGN KEY (`gallery_id`) REFERENCES `gallery` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
